#! /bin/bash
lxsession &
picom &
#nitrogen --restore &
feh --randomize --bg-scale ~/wallpapers
/usr/bin/variety
flameshot &
ibus-daemon -drxR &
volumeicon &
slstatus &
xautolock -time 10 -locker slock -nowlocker slock -detectsleep -corners 000+ -cornerdelay 3 &
play-with-mpv &
conky &
